import smbus


instance = None


class _I2CHandler:
    def __init__(self):
        self.bus = smbus.SMBus(1)

    def read_data(self, addr, mode):
        data = self.bus.read_i2c_block_data(addr, mode)
        return data


def I2CHandler():
    global instance
    if instance is None:
        instance = _I2CHandler()
    return instance
