from car_motors_controller import CarMotorController

class DriveController:
    def __init__(self, udp):
        self.motors_controller = CarMotorController()
        self.speed = None
        self.stop = None
        self.udp = udp
        self.directions = {}

    def drive(self):
        self.user_drive_mode()

    def user_drive_mode(self):
        go = self.determine_direction()
        if go:
            go(self.speed)
        else:
            self.motors_controller.stop()

    # TASK#6.1 create determine_direction()

    # TASK#5 create read_data_from_udp() 
    # - data from smartphone
    # - speed
    # - stop
    # - directions

    # TASK#6.2 create update method
    # remeber to get data from udp server
    # stop()- motors stop
    # drive()- car is moving in particular direction
        


if __name__ == "__main__":
    """Prototyping
    """
