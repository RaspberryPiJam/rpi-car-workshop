import socketserver
import threading
from light_sensor import LightSensor
from drive_controller import DriveController
from car_motors_controller import CarMotorController


class UdpReqHandler(
        socketserver.BaseRequestHandler):

    # TASK#1 create method setup() and handle()

    # TASK#4 create notify() - after assigning new values, notify DriveController

    # TASK#3 create method which is responsible for assigning new values


class UdpServer(
        socketserver.ThreadingMixIn,
        socketserver.UDPServer):
    pass


class UDPCarHandler:
    def __init__(self, udp):
        self.udp = udp
        self.drive_controller = DriveController(self)

    def read_data_from_smartphone(self):
        return self.udp.data_from_smartphone

    
    # TASK#4 send notification to controller 
