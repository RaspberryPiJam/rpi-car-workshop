from i2c_handler import I2CHandler


DEVICE_ADD_LOW = 0x23
DEVICE_ADD_HIGH = 0x5c
ONE_TIME_HIGH_RES_MODE = 0x20
CONT_LOW_RES_MODE = 0x13
MAX_READ_VALUE = 10000


class LightSensor:
    def __init__(self, sensor_i2c_add, read_mode=CONT_LOW_RES_MODE):
        self.i2c_handler = I2CHandler()
        self.sensor_i2c_add = sensor_i2c_add
        self.read_mode = read_mode

    @staticmethod
    def convert_sensor_data_to_decimal_bh1750(data):
        """
        Function which converts data binary data from read from sensor to decimal value
            formula copyed from official sensor's data sheet:
        How to calculate when the data High Byte is "10000011" and Low Byte is "10010000"
        ( 2^15 + 2^9 + 2^8 + 2^7 + 2^4) / 1.2 = 28067 [ lx ]
        Arguments:
            data {[list]} -- data from i2c_handler: list of bytes bytes' values  [high_byte, low_byte...]
            data[0] = high_byte, data[1] = low_byte
        Return:
            [float] - decimal value from light sensor in lux units, value should not be higher than MAX_READ_VALUE!
        """
        # participant implementation
        # hint 2<<1 = 4 2<<2 = 8 ;)
        read_value = ((int(data[0]) << 8) + data[1])/1.2
        if read_value < MAX_READ_VALUE:
            return read_value
        else:
            return MAX_READ_VALUE

    def read_sensor_light_value(self):
        self.i2c_handler.read_data(self.sensor_i2c_add, self.read_mode)
        data = self.i2c_handler.read_data(
            self.sensor_i2c_add, self.read_mode)
        return LightSensor.convert_sensor_data_to_decimal_bh1750(data)

    @classmethod
    def get_light_sensors(cls):
        """Get two sensors instance, left should be returend as first tuple value

        Returns:
            [tuple] -- [0]-left sensor, [1]- right sensor, PLease make sure the proper sensor is 
            retunred in good position
        """
        return cls(DEVICE_ADD_LOW), cls(DEVICE_ADD_HIGH)


if __name__ == "__main__":
    """
    Here we can test our code and for example in oroder to 
    determine which address is related to the particular sensor
    """
