import explorerhat
import time


class CarMotorController:
    def __init__(self):
        self.ratio = 0.7
        self.motor_left = explorerhat.motor.two
        self.motor_right = explorerhat.motor.one

    def move_forward(self, speed):
        self.motor_right.forward(speed)
        self.motor_left.forward(speed)

    def move_backward(self, speed):
        self.motor_right.backward(speed)
        self.motor_left.backward(speed)

    def move_right(self, speed):
        self.motor_right.stop()
        self.motor_left.forward(speed)

    def move_left(self, speed):
        self.motor_right.forward(speed)
        self.motor_left.stop()

    def move_left_forward(self, speed):
        self.motor_right.forward(speed*self.ratio)
        self.motor_left.forward(speed*(1-self.ratio))

    def move_left_backward(self, speed):
        self.motor_right.backward(speed*self.ratio)
        self.motor_left.backward(speed*(1-self.ratio))

    def move_right_forward(self, speed):
        self.motor_right.forward(speed*(1-self.ratio))
        self.motor_left.forward(speed*self.ratio)

    def move_right_backward(self, speed):
        self.motor_right.backward(speed*(1-self.ratio))
        self.motor_left.backward(speed*self.ratio)

    def control_two_motors(self, speed_motor_r, speed_motor_l):
        self.motor_right.forward(speed_motor_r)
        self.motor_left.forward(speed_motor_l)

    def stop(self):
        self.motor_right.stop()
        self.motor_left.stop()


if __name__ == "__main__":
    # dopisac metody w klasie CarMotorController
    # odpowiadajaca za zatrzymanie silnikow (stop)
    # odpowiadajaca za jazde w prawo  (move_right)
    # odpowiadajaca za jazde do tylu (move_backward)
    # odpowiadajace za jednoczesna jazde w dwoch mozliwych kierunkach, zastanowic sie nad stosunkiem predkosci, na danym kole:
    # w prawo i przod  (move_right_forward)
    # w lewo i przod  (move_left_forward)
    # w prawo i do tylu (move_right_backward)
    # w lewo i do tylu (move_left_backward)
    # stworzyc instancje CarMotorController w sekcji __name__ == "__main__",
    # skrecic w lewo przez 2s z predkoscia rowna 20,
    # skrecic w prawo przez 2s z predokoscia rowna 20,
    # sprwadzic czy odpowiednie silniki zostaly uruchomione i czy obracaja sie w dobrym kierunku
    print("prototyping")
