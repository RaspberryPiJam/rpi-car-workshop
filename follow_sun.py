from light_sensor import LightSensor
from car_motors_controller import CarMotorController
from time import sleep
from simple_pid import PID


left_sensor, right_sensor = LightSensor.get_light_sensors()
car_motors_controller = CarMotorController()
SPEED = 40


def calculate_error():
    return left_sensor.read_sensor_light_value() - right_sensor.read_sensor_light_value()


def if_based_controller():
    offset = 200
    while True:
        error = calculate_error()
        print(error)
        if error > 0 and error > offset/2:
            car_motors_controller.move_right(SPEED)
        elif error < 0 and error < -offset/2:
            car_motors_controller.move_left(SPEED)
        else:
            car_motors_controller.move_forward(SPEED)
        sleep(1)


def pid_based_controller():
    error_scale = 0.01  # Upper limit ouf PID output/MAX SENSOR READ VALUE
    pid = PID(Kp=1.8, Ki=0.1, Kd=0.8, setpoint=0,
              output_limits=(-100, 100))
    while True:
        error = calculate_error() * error_scale
        print("error scaled: {}".format(error))
        pid_output = pid(error)
        if pid_output >= 0:
            l_m = SPEED/(2.0 - pid_output/100)
            r_m = SPEED - l_m
            car_motors_controller.control_two_motors(r_m, l_m)
        else:
            r_m = SPEED/(2.0 + pid_output/100)
            l_m = SPEED - r_m
            print("R_M {}".format(r_m))
            print("L_M {}".format(l_m))
            car_motors_controller.control_two_motors(r_m, l_m)
        sleep(1)


while True:
    pid_based_controller()

car_motors_controller.stop()
